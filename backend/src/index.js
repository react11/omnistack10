const express = require('express');
const app = express();
const mongoose = require('mongoose')
const routes = require('./routes')

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/omnistack10', 
                {useNewUrlParser:true})


app.use(express.json())
app.use(routes)
app.listen(3333)